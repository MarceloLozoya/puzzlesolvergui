/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzlesolver;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author marcelo
 */
public class GUIController implements Initializable {
    
    private static final int numRows = 4;
    private static final int numCols = 5;
    private static final String errorLabelDefault = "  Drag all remaining pieces onto the game board";
    private static final String aColorStyle = "-fx-background-color: blue;";
    private static final String bColorStyle = "-fx-background-color: yellow;";
    private static final String cColorStyle = "-fx-background-color: green;";
    private static final String dColorStyle = "-fx-background-color: red;";
    private static final String redColorStyle = "-fx-background-color: red;";
    private static final String blackColorStyle = "-fx-background-color: black;";
    private static final String whiteColorStyle = "-fx-background-color: white;";
    private static final String yellowColorStyle = "-fx-background-color: yellow;";
    private static final String blueColorStyle = "-fx-background-color: blue;";
    private static final String greenColorStyle = "-fx-background-color: green;";
    private static final char nullChar = '\u0000';
    private int rowCoord;
    private int colCoord;
    private Integer aPieces = 4;
    private Integer bPieces = 1;
    private Integer cPieces = 1;
    private Integer dPieces = 4;
    private String aID;
    private String bID;
    private String cID;
    private String dID;
    
    @FXML private GridPane grid;
    @FXML private ImageView aImage;
    @FXML private ImageView bImage;
    @FXML private ImageView cImage;
    @FXML private ImageView dImage;
    @FXML private Label aLabel;
    @FXML private Label bLabel;
    @FXML private Label cLabel;
    @FXML private Label dLabel;
    @FXML private Label errorLabel;
    @FXML private Button solveBtn;
    @FXML private Button resetBtn;
    @FXML private MenuItem docMenuItem;
    
    private Pane[][] paneArray;
    private String[][] prevColorArr;
    private char[][] configArray;
    private Button[][] removeBtnArray;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        paneArray = new Pane[numRows][numCols];
        prevColorArr = new String[numRows][numCols];
        configArray = new char[numRows][numCols];
        removeBtnArray = new Button[numRows][numCols];
        
        aLabel.setTextFill(Color.WHITE);
        bLabel.setTextFill(Color.WHITE);
        cLabel.setTextFill(Color.WHITE);
        dLabel.setTextFill(Color.WHITE);
        aLabel.setText(aPieces + " Remaining");
        bLabel.setText(bPieces + " Remaining");
        cLabel.setText(cPieces + " Remaining");
        dLabel.setText(dPieces + " Remaining");
        
        aID = aImage.getImage().toString();
        bID = bImage.getImage().toString();
        cID = cImage.getImage().toString();
        dID = dImage.getImage().toString();
        
        errorLabel.setText(errorLabelDefault);
        errorLabel.setStyle(blackColorStyle);
        errorLabel.setTextFill(Color.WHITE);
        
        solveBtn.setDisable(true);
        
        registerSolveClicked(solveBtn);
        registerSolveEnterKey(solveBtn);
        registerResetEnterKey(resetBtn);
        registerDocMenuItem(docMenuItem);
        
        registerDragDetected(aImage);
        registerDragDetected(bImage);
        registerDragDetected(cImage);
        registerDragDetected(dImage);
        
        for(int i=0; i<numRows; i++){
            for(int j=0; j<numCols; j++){
                Pane pane = new Pane();
                pane.setOpacity(0.5);
                pane.setStyle(whiteColorStyle);
                registerDragOver(pane);
                registerDragEntered(pane);
                registerDragExited(pane);
                registerDragDropped(pane);
                paneArray[i][j] = pane;
                grid.add(paneArray[i][j], j, i);
            }
        }
        
    }
    
    @FXML private void killSolver(){
        System.exit(0);
    }
    
    @FXML private void resetBtnPressed(){
        for(int i=0; i<numRows; i++){
            for(int j=0; j<numCols; j++){
                paneArray[i][j].setStyle(whiteColorStyle);
                configArray[i][j] = nullChar;
                if(removeBtnArray[i][j] != null)
                    grid.getChildren().remove(removeBtnArray[i][j]);
            }
        }
        resetPieceLabels();
        solveBtn.setDisable(true);
        setErrorLabel(blackColorStyle, errorLabelDefault);
    }
    
    private void registerSolveEnterKey(Button btn){
        btn.setOnKeyPressed((KeyEvent key) -> {
            if(key.getCode().equals(KeyCode.ENTER))
                try {
                    setErrorLabel(greenColorStyle, "...Solving Puzzle...");
                    solvePuzzle();
            } catch (InterruptedException ex) {
                Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    private void registerSolveClicked(Button btn){
        btn.setOnAction((ActionEvent event) -> {
            try {
                setErrorLabel(greenColorStyle, "...Solving Puzzle...");
                solvePuzzle();
            } catch (InterruptedException ex) {
                Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
    
    private void registerResetEnterKey(Button btn){
        btn.setOnKeyPressed((KeyEvent key) -> {
            if(key.getCode().equals(KeyCode.ENTER))
                resetBtnPressed();
        });
    }
    
    private void registerDocMenuItem(MenuItem item){
        item.setOnAction(e -> {
            VBox vBox = new VBox();
            ImageView view1 = new ImageView();
            ImageView view2 = new ImageView();
            view1.setPreserveRatio(true);
            view2.setPreserveRatio(true);
            view1.setFitWidth(800);
            view2.setFitWidth(800);
            
            Image pageOneImage = new Image(getClass().getResourceAsStream("PuzzleDoc1.jpg"));
            Image pageTwoImage = new Image(getClass().getResourceAsStream("PuzzleDoc2.jpg"));
            view1.setImage(pageOneImage);
            view2.setImage(pageTwoImage);
            
            
            ScrollPane newPane = new ScrollPane();
            newPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
            newPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
            newPane.setContent(null);
            newPane.setContent(vBox);
            
            vBox.getChildren().addAll(view1, view2);
            Scene newScene = new Scene(newPane, 800, 1000);
            Stage newStage = new Stage();
            newStage.setTitle("How The Game Works");
            newStage.setScene(newScene);
            newStage.setResizable(false);
            newStage.show();
        });
    }
    
    private void registerDragDetected(ImageView view){
        view.setOnDragDetected((MouseEvent event) -> {
            if (getPiecesRemaining(view) > 0) {
                setErrorLabel(blackColorStyle, errorLabelDefault);
                Dragboard db = view.startDragAndDrop(TransferMode.ANY);
                ClipboardContent content = new ClipboardContent();
                Image image = view.getImage();
                content.putString(getContentString(view));
                db.setContent(content);

                double xOffset = (image.getWidth() / 2) * 0.8;
                double yOffset = (image.getHeight() / 2) * -0.8;

                db.setDragView(image, xOffset, yOffset);
                event.consume();
            }
        });
    }
    
    private void registerDragOver(Pane pane){
        pane.setOnDragOver((DragEvent event)->{
            if(event.getDragboard().hasString())
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            event.consume();
        });
    }
    
    private void registerDragEntered(Pane pane){
        pane.setOnDragEntered((DragEvent event)->{
            if(event.getDragboard().hasString()){
                outerloop:
                for(int i=0; i<numRows; i++){
                    for(int j=0; j<numCols; j++){
                        if(pane == paneArray[i][j]){
                            rowCoord = i;
                            colCoord = j;
                            break outerloop;
                        }
                    }
                }
                String pieceType = event.getDragboard().getString();
                highlightSquares(rowCoord, colCoord, pieceType);
            }
            event.consume();
        });
    }
    
    private void registerDragExited(Pane pane){
        pane.setOnDragExited((DragEvent event)->{
            if(event.getDragboard().hasString()){
                outerloop:
                for(int i=0; i<numRows; i++){
                    for(int j=0; j<numCols; j++){
                        if(pane == paneArray[i][j]){
                            rowCoord = i;
                            colCoord = j;
                            break outerloop;
                        }
                    }
                }
                
                String pieceType = event.getDragboard().getString();
                removeHighlighting(rowCoord, colCoord, pieceType);
                colorGrid();
            }
            event.consume();
        });
    }
    
    private void registerDragDropped(Pane pane){
        pane.setOnDragDropped((DragEvent event)->{
            if(event.getDragboard().hasString()){
                outerloop:
                for(int i=0; i<numRows; i++){
                    for(int j=0; j<numCols; j++){
                        if(pane == paneArray[i][j]){
                            rowCoord = i;
                            colCoord = j;
                            break outerloop;
                        }
                    }
                }
                
                String pieceType = event.getDragboard().getString();
                setPieceOntoBoard(rowCoord, colCoord, pieceType);
            }
        });
    }
    
    private String getContentString(ImageView view){
        String viewID = view.getImage().toString();
        
        if(viewID.equalsIgnoreCase(aID))
            return "A";
        else if(viewID.equalsIgnoreCase(bID))
            return "B";
        else if(viewID.equalsIgnoreCase(cID))
            return "C";
        else
            return "D";
    }
    
    private int getPiecesRemaining(ImageView view){
        String viewID = view.getImage().toString();
        
        if(viewID.equalsIgnoreCase(aID))
            return aPieces;
        else if(viewID.equalsIgnoreCase(bID))
            return bPieces;
        else if(viewID.equalsIgnoreCase(cID))
            return cPieces;
        else
            return dPieces;
    }
    
    private char getPieceType(int row, int col){
        switch(configArray[row][col]){
            case 'A': return 'A';
            case 'B': return 'B';
            case 'C': return 'C';
            case 'D': return 'D';
            default: return nullChar;
        }
    }
    
    private String getMoveDirection(String instruction){
        String move = instruction.substring(instruction.lastIndexOf(" ")+1);
        return move;
    }
    
    private void highlightSquares(int row, int col, String pieceType){
        if(pieceType.equals("A")){
            if(col == (numCols-1)){
                prevColorArr[row][col] = paneArray[row][col].getStyle();
                paneArray[row][col].setStyle(aColorStyle);
            }
            else{
                prevColorArr[row][col] = paneArray[row][col].getStyle();
                prevColorArr[row][col+1] = paneArray[row][col+1].getStyle();
                paneArray[row][col].setStyle(aColorStyle);
                paneArray[row][col+1].setStyle(aColorStyle);
            }
        }
        else if(pieceType.equals("B")){
            if(row == (numRows-1)){
                prevColorArr[row][col] = paneArray[row][col].getStyle();
                paneArray[row][col].setStyle(bColorStyle);
            }
            else{
                prevColorArr[row][col] = paneArray[row][col].getStyle();
                prevColorArr[row+1][col] = paneArray[row+1][col].getStyle();
                paneArray[row][col].setStyle(bColorStyle);
                paneArray[row+1][col].setStyle(bColorStyle);
            }
        }
        else if(pieceType.equals("C")){
            if(row==(numRows-1) && col==(numCols-1)){
                prevColorArr[row][col] = paneArray[row][col].getStyle();
                paneArray[row][col].setStyle(cColorStyle);
            }
            else if(row == (numRows-1)){
                prevColorArr[row][col] = paneArray[row][col].getStyle();
                prevColorArr[row][col+1] = paneArray[row][col+1].getStyle();
                paneArray[row][col].setStyle(cColorStyle);
                paneArray[row][col+1].setStyle(cColorStyle);
            }
            else if(col == (numCols-1)){
                prevColorArr[row][col] = paneArray[row][col].getStyle();
                prevColorArr[row+1][col] = paneArray[row+1][col].getStyle();
                paneArray[row][col].setStyle(cColorStyle);
                paneArray[row+1][col].setStyle(cColorStyle);
            }
            else{
                prevColorArr[row][col] = paneArray[row][col].getStyle();
                prevColorArr[row+1][col] = paneArray[row+1][col].getStyle();
                prevColorArr[row][col+1] = paneArray[row][col+1].getStyle();
                prevColorArr[row+1][col+1] = paneArray[row+1][col+1].getStyle();
                paneArray[row][col].setStyle(cColorStyle);
                paneArray[row+1][col].setStyle(cColorStyle);
                paneArray[row][col+1].setStyle(cColorStyle);
                paneArray[row+1][col+1].setStyle(cColorStyle);
            }
        }
        else if(pieceType.equals("D")){
            prevColorArr[row][col] = paneArray[row][col].getStyle();
            paneArray[row][col].setStyle(dColorStyle);
        }
    }
    
    private void removeHighlighting(int row, int col, String pieceType){
        if(pieceType.equals("A")){
            if(col == (numCols-1)){
                paneArray[row][col].setStyle(prevColorArr[row][col]);
            }
            else{
                paneArray[row][col].setStyle(prevColorArr[row][col]);
                paneArray[row][col+1].setStyle(prevColorArr[row][col+1]);
            }
        }
        else if(pieceType.equals("B")){
            if(row == (numRows-1)){
                paneArray[row][col].setStyle(prevColorArr[row][col]);
            }
            else{
                paneArray[row][col].setStyle(prevColorArr[row][col]);
                paneArray[row+1][col].setStyle(prevColorArr[row+1][col]);
            }
        }
        else if(pieceType.equals("C")){
            if(row == (numRows-1) && col == (numCols-1)){
                paneArray[row][col].setStyle(prevColorArr[row][col]);
            }
            else if(col == (numCols-1)){
                paneArray[row][col].setStyle(prevColorArr[row][col]);
                paneArray[row+1][col].setStyle(prevColorArr[row+1][col]);
            }
            else if(row == (numRows-1)){
                paneArray[row][col].setStyle(prevColorArr[row][col]);
                paneArray[row][col+1].setStyle(prevColorArr[row][col+1]);
            }
            else{
                paneArray[row][col].setStyle(prevColorArr[row][col]);
                paneArray[row+1][col].setStyle(prevColorArr[row+1][col]);
                paneArray[row][col+1].setStyle(prevColorArr[row][col+1]);
                paneArray[row+1][col+1].setStyle(prevColorArr[row+1][col+1]);
            }
        }
        else if(pieceType.equals("D")){
            paneArray[row][col].setStyle(prevColorArr[row][col]);
        }
    }
    
    private void setPieceOntoBoard(int row, int col, String pieceType){
        String noFit = "  *Piece doesn't fit at that location";
        String occupied = "  *Required space(s) already occupied";
        
        if(pieceType.equals("A")){
            if(col == (numCols-1)){
                setErrorLabel(redColorStyle, noFit);
            }
            else if(configArray[row][col] != nullChar ||
                    configArray[row][col+1] != nullChar){
                setErrorLabel(redColorStyle, occupied);
            }
            else{
                configArray[row][col] = 'A';
                configArray[row][col+1] = 'A';
                aPieces--;
                setPieceLabel(configArray[row][col]);
                setRemoveButton(row, col);
            }
        }
        else if(pieceType.equals("B")){
            if(row == (numRows-1)){
                setErrorLabel(redColorStyle, noFit);
            }
            else if(configArray[row][col] != nullChar ||
                    configArray[row+1][col] != nullChar){
                setErrorLabel(redColorStyle, occupied);
            }
            else{
                configArray[row][col] = 'B';
                configArray[row+1][col] = 'B';
                bPieces--;
                setPieceLabel(configArray[row][col]);
                setRemoveButton(row, col);
            }
        }
        else if(pieceType.equals("C")){
            if(row==(numRows-1) || col==(numCols-1)){
                setErrorLabel(redColorStyle, noFit);
            }
            else if(configArray[row][col] != nullChar ||
                    configArray[row+1][col] != nullChar ||
                    configArray[row][col+1] != nullChar ||
                    configArray[row+1][col+1] != nullChar){
                setErrorLabel(redColorStyle, occupied);
            }
            else{
                configArray[row][col] = 'C';
                configArray[row+1][col] = 'C';
                configArray[row][col+1] = 'C';
                configArray[row+1][col+1] = 'C';
                cPieces--;
                setPieceLabel(configArray[row][col]);
                setRemoveButton(row, col);
            }
        }
        else if(pieceType.equals("D")){
            if(configArray[row][col] != nullChar){
                setErrorLabel(redColorStyle, occupied);
            }
            else{
                configArray[row][col] = 'D';
                dPieces--;
                setPieceLabel(configArray[row][col]);
                setRemoveButton(row, col);
            }
        }
        
        if(isBoardSolvable())
            solveBtn.setDisable(false);
    }
    
    private void colorGrid(){
        for(int i=0; i<numRows; i++){
            for(int j=0; j<numCols; j++){
                if(configArray[i][j] == 'A'){
                    paneArray[i][j].setStyle(aColorStyle);
                }
                else if(configArray[i][j] == 'B'){
                    paneArray[i][j].setStyle(bColorStyle);
                }
                else if(configArray[i][j] == 'C'){
                    paneArray[i][j].setStyle(cColorStyle);
                }
                else if(configArray[i][j] == 'D'){
                    paneArray[i][j].setStyle(dColorStyle);
                }
                else{
                    paneArray[i][j].setStyle(whiteColorStyle);
                }
            }
        }
    }
    
    private void setErrorLabel(String colorStyle, String text){
        errorLabel.setText(text);
        errorLabel.setStyle(colorStyle);
    }
    
    private void resetPieceLabels(){
        aPieces = 4;
        bPieces = 1;
        cPieces = 1;
        dPieces = 4;
        aLabel.setText(aPieces + " Remaining");
        bLabel.setText(bPieces + " Remaining");
        cLabel.setText(cPieces + " Remaining");
        dLabel.setText(dPieces + " Remaining");
    }
    
    private void setPieceLabel(char pieceChar){
        switch(pieceChar){
            case 'A' : 
                aLabel.setText(aPieces+" Remaining");
                break;
            case 'B' : 
                bLabel.setText(bPieces+" Remaining");
                break;
            case 'C' : 
                cLabel.setText(cPieces+" Remaining");
                break;
            case 'D' : 
                dLabel.setText(dPieces+" Remaining");
                break;
            default:
                break;
        }
    }
    
    private void setRemoveButton(int row, int col){
        Button btn = new Button();
        btn.setText("X");
        btn.setOnAction((ActionEvent event) -> {
            removePiece(row, col, btn);
        });
        grid.add(btn, col, row);
        GridPane.setValignment(btn, VPos.TOP);
        removeBtnArray[row][col] = btn;
    }
    
    private void removePiece(int row, int col, Button btn){
        grid.getChildren().remove(btn);
        solveBtn.setDisable(true);
        
        if(configArray[row][col] == 'A'){
            aPieces++;
            setPieceLabel(configArray[row][col]);
            paneArray[row][col].setStyle(whiteColorStyle);
            paneArray[row][col+1].setStyle(whiteColorStyle);
            configArray[row][col] = nullChar;
            configArray[row][col+1] = nullChar;
        }
        else if(configArray[row][col] == 'B'){
            bPieces++;
            setPieceLabel(configArray[row][col]);
            paneArray[row][col].setStyle(whiteColorStyle);
            paneArray[row+1][col].setStyle(whiteColorStyle);
            configArray[row][col] = nullChar;
            configArray[row+1][col] = nullChar;
        }
        else if(configArray[row][col] == 'C'){
            cPieces++;
            setPieceLabel(configArray[row][col]);
            paneArray[row][col].setStyle(whiteColorStyle);
            paneArray[row+1][col].setStyle(whiteColorStyle);
            paneArray[row][col+1].setStyle(whiteColorStyle);
            paneArray[row+1][col+1].setStyle(whiteColorStyle);
            configArray[row][col] = nullChar;
            configArray[row+1][col] = nullChar;
            configArray[row][col+1] = nullChar;
            configArray[row+1][col+1] = nullChar;
        }
        else if(configArray[row][col] == 'D'){
            dPieces++;
            setPieceLabel(configArray[row][col]);
            paneArray[row][col].setStyle(whiteColorStyle);
            configArray[row][col] = nullChar;
        }
    }
    
    private boolean isBoardSolvable(){
        return aPieces==0 && bPieces==0 && cPieces==0 && dPieces==0;
    }
    
    private void clearRemoveButtons(){
        for(int i=0; i<numRows; i++){
            for(int j=0; j<numCols; j++){
                if(removeBtnArray[i][j] != null)
                    grid.getChildren().remove(removeBtnArray[i][j]);
            }
        }
    }
    
    private void finalizeConfigArray(){
        for(int i=0; i<numRows; i++){
            for(int j=0; j<numCols; j++){
                if(configArray[i][j] == nullChar)
                    configArray[i][j] = 'E';
            }
        }
    }
    
    private char[][] copyConfig(char[][] config){
        char[][] copy = new char[numRows][numCols];
        
        for(int i=0; i<numRows; i++){
            for(int j=0; j<numCols; j++){
                copy[i][j] = config[i][j];
            }
        }
        return copy;
    }
    
    private void animatePiece(int row,int col,char pieceType,String direction) throws InterruptedException{
        
        if(pieceType == 'A'){
            if(direction.equalsIgnoreCase("up")){
                configArray[row-1][col] = 'A';
                configArray[row-1][col+1] = 'A';
                configArray[row][col] = 'E';
                configArray[row][col+1] = 'E';
            }
            else if(direction.equalsIgnoreCase("right")){
                configArray[row][col] = 'E';
                configArray[row][col+2] = 'A';
            }
            else if(direction.equalsIgnoreCase("down")){
                configArray[row+1][col] = 'A';
                configArray[row+1][col+1] = 'A';
                configArray[row][col] = 'E';
                configArray[row][col+1] = 'E';
            }
            else{
                configArray[row][col-1] = 'A';
                configArray[row][col+1] = 'E';
            }
        }
        else if(pieceType == 'B'){
            if(direction.equalsIgnoreCase("up")){
                configArray[row-1][col] = 'B';
                configArray[row+1][col] = 'E';
            }
            else if(direction.equalsIgnoreCase("right")){
                configArray[row][col+1] = 'B';
                configArray[row+1][col+1] = 'B';
                configArray[row][col] = 'E';
                configArray[row+1][col] = 'E';
            }
            else if(direction.equalsIgnoreCase("down")){
                configArray[row+2][col] = 'B';
                configArray[row][col] = 'E';
            }
            else{
                configArray[row][col-1] = 'B';
                configArray[row+1][col-1] = 'B';
                configArray[row][col] = 'E';
                configArray[row+1][col] = 'E';
            }
        }
        else if(pieceType == 'C'){
            if(direction.equalsIgnoreCase("up")){
                configArray[row-1][col] = 'C';
                configArray[row-1][col+1] = 'C';
                configArray[row+1][col] = 'E';
                configArray[row+1][col+1] = 'E';
            }
            else if(direction.equalsIgnoreCase("right")){
                configArray[row][col+2] = 'C';
                configArray[row+1][col+2] = 'C';
                configArray[row][col] = 'E';
                configArray[row+1][col] = 'E';
            }
            else if(direction.equalsIgnoreCase("down")){
                configArray[row+2][col] = 'C';
                configArray[row+2][col+1] = 'C';
                configArray[row][col] = 'E';
                configArray[row][col+1] = 'E';
            }
            else{
                configArray[row][col-1] = 'C';
                configArray[row+1][col-1] = 'C';
                configArray[row][col+1] = 'E';
                configArray[row+1][col+1] = 'E';
            }
        }
        else{
            if(direction.equalsIgnoreCase("up")){
                configArray[row-1][col] = 'D';
                configArray[row][col] = 'E';
            }
            else if(direction.equalsIgnoreCase("right")){
                configArray[row][col+1] = 'D';
                configArray[row][col] = 'E';
            }
            else if(direction.equalsIgnoreCase("down")){
                configArray[row+1][col] = 'D';
                configArray[row][col] = 'E';
            }
            else{
                configArray[row][col-1] = 'D';
                configArray[row][col] = 'E';
            }
        }
        colorGrid();
    }
    
    private void solvePuzzle() throws InterruptedException{
        solveBtn.setDisable(true);
        resetBtn.setDisable(true);
        
        finalizeConfigArray();
        AlgorithmsProject solver = new AlgorithmsProject();
        solver.startSolver(copyConfig(configArray));
        System.out.println(solver.moveStack.toString());
        clearRemoveButtons();
        
        Task task = new Task<Void>() {
            @Override
            public Void call() throws Exception{
                while(!solver.moveStack.isEmpty()){
                    Thread.sleep(1000);
                    String move = solver.moveStack.pop();
                    String moveDirection = getMoveDirection(move);
                    move = move.substring(move.indexOf("(")+1, move.indexOf(")"));
                    int i = Character.getNumericValue(move.charAt(0));
                    int j = Character.getNumericValue(move.charAt(2));
            
                    char pieceType = getPieceType(i,j);
                    Platform.runLater(() -> {
                        try {
                            animatePiece(i,j,pieceType, moveDirection);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(GUIController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    });
                }
                return null;
            }
        };
        
        task.setOnSucceeded(e -> {
            resetBtn.setDisable(false);
            setErrorLabel(blueColorStyle, "***Here is the winning configuration***");
        });
        
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }
    
}
