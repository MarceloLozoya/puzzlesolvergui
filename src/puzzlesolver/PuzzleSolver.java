/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package puzzlesolver;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

/**
 *
 * @author marcelo
 */
public class PuzzleSolver extends Application {
    
    private static Stage gui;
    
    @Override
    public void start (Stage primaryStage) throws IOException {
        gui = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("GUI.fxml"));
        Scene scene = new Scene(root, 600, 570);
        
        gui.setTitle("Wooden Box Puzzle Solver");
        gui.setScene(scene);
        gui.setResizable(false);
        gui.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
